local function printUsage()
  print("Usage:")
  print("git <repo> <file> <branch>")
end

local tArgs = { ... }
if #tArgs < 1 then
  printUsage()
  return
end

if not http then
  printError("git requires http API")
  return
end

local function get(sUrl)
  write("Connecting to " .. sUrl .. "... ")

  local ok, err = http.checkURL(sUrl)
  if not ok then
    print("Failed.")
    if err then
      printError(err)
    end
    return nil
  end

  local response = http.get(sUrl, nil, true)
  if not response then
    print("Failed.")
    return nil
  end

  print("Success.")

  local sResponse = response.readAll()
  response.close()
  return sResponse
end

local sRepo = tArgs[1]
local sFile = tArgs[2] and tArgs[2] or "build.lua"
local sBranch = tArgs[3] and tArgs[3] or "master"
local sUrl = "https://gitlab.com/discraft/" .. sRepo .. "/-/raw/" .. sBranch .. "/" .. sFile
local fileName = sFile == "build.lua" and sRepo or sFile
local sPath = shell.resolve(fileName)

if fs.exists(fileName) then
  print("Removing existing version...")
  fs.delete(fileName)
end

local res = get(sUrl)
if res then
  local file = fs.open(fileName, "wb")
  file.write(res)
  file.close()

  print("Added as " .. fileName)
end
